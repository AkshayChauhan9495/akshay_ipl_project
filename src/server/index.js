const fs = require('fs');
const csv = require('csv-parser');
const {
    matchesPlayedPerYear,
    matchesWonPerTeamPerYear,
    extraRunsConcededPerTeam,
    top10EconomicalBowlers2015
} = require('./ipl');

const deliveries = [];
const matches = [];

fs.createReadStream('../../src/data/deliveries.csv')
    .pipe(csv())
    .on('data', (data) => deliveries.push(data))
    .on('end', () => {

        fs.createReadStream('../../src/data/matches.csv')
            .pipe(csv())
            .on('data', (data) => matches.push(data))
            .on('end', () => {

                const matchesPerYear = matchesPlayedPerYear(matches);
                const wonMatchPerTeamPerYear = matchesWonPerTeamPerYear(matches);
                const extraRunsConceded = extraRunsConcededPerTeam(matches, deliveries);
                const top10EconomicalBowlers = top10EconomicalBowlers2015(matches, deliveries);

                function sendOutputData(file, data) {
                    const error = (err) => {
                        if (err) {
                            console.log(err);
                            return;
                        }
                    };

                    const jsonData = JSON.stringify(data, null, 8);
                    fs.writeFile(file, jsonData, error);

                }
                sendOutputData('../../src/public/output/matchesPerYear.json', matchesPerYear);
                sendOutputData('../../src/public/output/wonMatchPerTeamPerYear.json', wonMatchPerTeamPerYear);
                sendOutputData('../../src/public/output/extraRunsConceded.json', extraRunsConceded);
                sendOutputData('../../src/public/output/top10EconomicalBowlers.json', top10EconomicalBowlers);
            });
    });
