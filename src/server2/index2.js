const fs = require('fs');
const csv = require('csv-parser');
const {
    tossAndMatchWin,
    manOfThematch,
    dismissalByPlayer,
    economicalBowlerSuper,
    strikeRateOfBatsman

} = require('./ipl2');

const deliveries = [];
const matches = [];

fs.createReadStream('../../src/data/deliveries.csv')
    .pipe(csv())
    .on('data', (data) => deliveries.push(data))
    .on('end', () => {

        fs.createReadStream('../../src/data/matches.csv')
            .pipe(csv())
            .on('data', (data) => matches.push(data))
            .on('end', () => {

                const tossAndMatchWinner = tossAndMatchWin(matches);
                const manOfThematchPlayer = manOfThematch(matches);
                const dismissedPlayer = dismissalByPlayer(deliveries);
                const economicalBowler = economicalBowlerSuper(deliveries);
                const strikeRateBatsman = strikeRateOfBatsman(matches, deliveries);

                function sendOutputData(file, data) {
                    const error = (err) => {
                        if (err) {
                            console.log(err);
                            return;
                        }
                    };

                    const jsonData = JSON.stringify(data, null, 8);
                    fs.writeFile(file, jsonData, error);

                }
                sendOutputData('../../src/public/output2/tossAndMatchWinner.json', tossAndMatchWinner);
                sendOutputData('../../src/public/output2/manOfThematchPlayer.json', manOfThematchPlayer);
                sendOutputData('../../src/public/output2/dismissedPlayer.json', dismissedPlayer);
                sendOutputData('../../src/public/output2/economicalBowler.json', economicalBowler);
                sendOutputData('../../src/public/output2/strikeRateBatsman.json', strikeRateBatsman);

            });
    });
