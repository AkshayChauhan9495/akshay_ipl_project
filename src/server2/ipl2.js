//This function is used to get the number of times each team won the toss and also won the match.

function tossAndMatchWin(matches) {

    let win = matches.reduce(function (win, value) {
        if (win.hasOwnProperty(value.toss_winner)) {

            if (value.toss_winner === value.winner) {
                win[value.toss_winner]++;
            }
        }
        else {

            if (value.toss_winner === value.winner) {
                win[value.toss_winner] = 1;
            }
        }

        return win;

    }, {})

    return win;
}

// This function is used to find a player who has won the highest number of Player of the Match awards for each season

function manOfThematch(matches) {

    let player = matches.reduce(function (player, value) {

        if (player.hasOwnProperty(value.season)) {

            if (player[value.season].hasOwnProperty(value.player_of_match)) {
                player[value.season][value.player_of_match]++;
            }

            else {
                player[value.season][value.player_of_match] = 1;
            }

        }

        else {
            player[value.season] = {}
            player[value.season][value.player_of_match] = 1;
        }
        return player;

    }, {})

    let sorted = Object.fromEntries(Object.entries(player).map((x) => ([x[0], Object.fromEntries(Object.entries(x[1]).sort((a, b) => a[1] - b[1]).reverse().filter((x, y) => y < 1)
    )])
    ));

    return sorted;
}

// This function is used to find the highest number of times one player has been dismissed by another player

function dismissalByPlayer(deliveries) {

    let dismissal = deliveries.reduce(function (dismissal, value) {

        if (value.batsman == value.player_dismissed && value.dismissal_kind !== "run out") {

            if (!dismissal.hasOwnProperty(value.batsman)) {
                dismissal[value.batsman] = {};
                dismissal[value.batsman][value.bowler] = 1;
            }
            else {

                if (!dismissal[value.batsman].hasOwnProperty(value.bowler)) {
                    dismissal[value.batsman][value.bowler] = 1;
                }

                else {
                    dismissal[value.batsman][value.bowler]++;
                }
            }
        }
        return dismissal;

    }, {})

    return (Object.fromEntries(Object.entries(dismissal).map(x => [x[0], Math.max(...Object.values(x[1]).sort((a, b) => a - b).reverse())]
    )));

}

//This function is used find the bowler with the best economy in super overs

function economicalBowlerSuper(deliveries) {

    let superOver = deliveries.reduce(function (superOver, value) {

        if (value.is_super_over == 1) {

            let total = Number(value.wide_runs) + Number(value.noball_runs) + Number(value.batsman_runs)

            if (!superOver.hasOwnProperty(value.bowler)) {
                superOver[value.bowler] = {};
                superOver[value.bowler].runs = total;
                superOver[value.bowler].balls = 1;

            }
            else {

                superOver[value.bowler].runs += (total);
                superOver[value.bowler].balls++;
            }
        }

        return superOver;

    }, {})
    let economicalSuperBowler = (Object.fromEntries(Object.entries(superOver).map((x, y) => [x[0], Number((x[1].runs / Number((x[1].balls) / 6)).toFixed(2))]).sort((a, b) => a[1] - b[1]).filter((x, y) => y < 1)));

    return economicalSuperBowler;
}

// This function is to find the strike rate of a batsman for each season


function strikeRateOfBatsman(matches, deliveries) {
    let matchId = Object.fromEntries(matches.map(x => [x.id, x.season]))

    let runsAndBalls = deliveries.reduce(function (runsAndBalls, value) {

        if (value.is_super_over == 0) {

            if (!runsAndBalls.hasOwnProperty(matchId[value.match_id])) {
                runsAndBalls[matchId[value.match_id]] = {};
                runsAndBalls[matchId[value.match_id]][value.batsman] = { "runs": Number(value.batsman_runs) }
                if (value.wide_runs > 0) {
                    runsAndBalls[matchId[value.match_id]][value.batsman].balls = 0;
                }
                else {
                    runsAndBalls[matchId[value.match_id]][value.batsman].balls = 1;
                }
            }
            else {
                if (!runsAndBalls[matchId[value.match_id]].hasOwnProperty(value.batsman)) {
                    runsAndBalls[matchId[value.match_id]][value.batsman] = { "runs": Number(value.batsman_runs) }

                    if (value.wide_runs > 0) {
                        runsAndBalls[matchId[value.match_id]][value.batsman].balls = 0;
                    }
                    else {
                        runsAndBalls[matchId[value.match_id]][value.batsman].balls = 1;
                    }
                }
                else {
                    runsAndBalls[matchId[value.match_id]][value.batsman].runs += Number(value.batsman_runs);
                    if (value.wide_runs == 0) {
                        runsAndBalls[matchId[value.match_id]][value.batsman].balls++;
                    }
                }
            }
        }
        return runsAndBalls;
    }, {})
    let stikeRate = Object.keys(runsAndBalls).reduce((acc, current1) => {

        acc[current1] = {};
        acc[current1] = Object.keys(runsAndBalls[current1]).reduce((acc, current2) => {

            acc[current2] = {};
            acc[current2] = Number(((runsAndBalls[current1][current2]["runs"] / runsAndBalls[current1][current2]["balls"]) * 100).toFixed(2));
            return acc;

        }, {})

        return acc;

    }, {})

    return stikeRate;
}



module.exports = { tossAndMatchWin, manOfThematch, dismissalByPlayer, economicalBowlerSuper, strikeRateOfBatsman }